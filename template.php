<?php
/**
 * @file
 * Module with functions for Forensische zorg theme.
 */

include_once "template-to-base.php";

/**
 * Implements hook_css_alter().
 * We strip out most default styles because they are only used in the back-end.
 */
function fz_rijkshuisstijl_css_alter(&$css) {
  uasort($css, 'drupal_sort_css_js');
  unset($css['misc/ui/jquery.ui.core.css']);
  unset($css['misc/ui/jquery.ui.theme.css']);
  unset($css[drupal_get_path('module', 'ctools') . '/css/ctools.css']);
  unset($css[drupal_get_path('module', 'date_api') . '/date.css']);
  unset($css[drupal_get_path('module', 'date_popup') . '/themes/datepicker.1.7.css']);
  unset($css[drupal_get_path('module', 'elf') . '/css/elf.css']);
  unset($css[drupal_get_path('module', 'user') . '/user.css']);
  unset($css[drupal_get_path('module', 'search') . '/search.css']);
  unset($css[drupal_get_path('module', 'search') . '/search.css']);
  unset($css[drupal_get_path('module', 'views') . '/css/views.css']);
  unset($css[drupal_get_path('module', 'webform') . '/css/webform.css']);
  unset($css[drupal_get_path('module', 'rijksvideo') . '/css/rijksvideo.theme.css']);
  unset($css[drupal_get_path('theme', 'rijkshuisstijl') . '/styles/application.css']);
  unset($css[drupal_get_path('theme', 'rijkshuisstijl') . '/styles/vertical-tabs.css']);

  // Use the IE8 stylesheet for IE9 as well.
  if (isset($css['sites/all/libraries/starterskit/css/ie/ie8.css'])) {
    $css['sites/all/libraries/starterskit/css/ie/ie8.css']['browsers']['IE'] = 'lte IE 9';
  }

  // What we have left, combine them all into one CSS group.
  $i = 0;
  foreach ($css as $name => $style) {
    $css[$name]['weight'] = $i++;
    $css[$name]['group'] = CSS_DEFAULT;
    $css[$name]['every_page'] = FALSE;
  }
}

/**
 * Implements hook_js_alter().
 * We strip out most default JS because they are only used in the back-end.
 */
function fz_rijkshuisstijl_js_alter(&$js) {
  unset($js['misc/ui/jquery.ui.core.min.js']);
  unset($js['misc/ui/jquery.ui.mouse.min.js']);
  unset($js['misc/ui/jquery.ui.widget.min.js']);
  unset($js['misc/ui/jquery.ui.draggable.min.js']);
  unset($js['misc/ui/jquery.ui.sortable.min.js']);
  unset($js['misc/ui/jquery.ui.droppable.min.js']);
}

/**
 * Change the breadcrumb for search results.
 */
function fz_rijkshuisstijl_breadcrumb($variables) {
  $breadcrumb = &$variables['breadcrumb'];
  if (!empty($breadcrumb) && isset($breadcrumb[2])) {
    if (arg(0) == 'search' && strip_tags($breadcrumb[2]) == t('Content')) {
      unset($breadcrumb[2]);
      drupal_set_title(t("Search results for '!term'", array('!term' => arg(2))));
    }
  }
  return rijkshuisstijl_breadcrumb($variables);
}

/**
 * Implements hook_status_messages().
 **/
function fz_rijkshuisstijl_status_messages($variables) {
  $display = $variables['display'];
  $output = '';
  $status_heading = array(
    'status'  => t('Info'),
    'error'   => t('Error'),
    'warning'   => t('Warning'),
    'notice'   => t('Notice'),
  );

  foreach (drupal_get_messages($display) as $type => $messages) {
    if ($type == 'error') {
      $typeextra = 'message_err';
    }
    else {
      $typeextra = 'message_info';
    }

    $output .= "<div class=\"mod $typeextra\">\n";
    if (count($messages) > 1) {
      $output .= '<h2>' . $status_heading[$type] . "</h2>\n";
      $output .= " <ul>\n";

      foreach ($messages as $message) {
        $output .= '  <li><p>' . $message . "</p></li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= '<h2>' . $messages[0] . "</h2>\n";
    }

    $output .= "</div>\n";
  }

  return $output;
}

/**
 * Implements theme_link().
 * Override links to add an 'is-external' class if needed.
 *
 * Only adds the is-external class to external links that
 * don't contain an image.
 */
function fz_rijkshuisstijl_link($variables) {
  global $base_url;
  if (
    // Is the URL absolute?
    url_is_external($variables['path']) &&
    // The site url is not part of the path.
    strstr($variables['path'], $base_url) === FALSE &&
    // The link does not contain an image (to prevent double images).
    strstr($variables['text'], '<img ') === FALSE
  ) {
    $variables['options']['attributes']['class'][] = 'is-external';
    $variables['options']['html'] = TRUE;
    $variables['text'] = '<span class="imgreplacement"></span>' . $variables['text'];

    if (isset($variables['options']['attributes']['title'])) {
      $title = &$variables['options']['attributes']['title'];
      $title = t('External site: @title', array('@title' => $title));
    }
    else {
      $variables['options']['attributes']['title'] = t('External site');
    }
  }
  return '<a href="' . check_plain(url($variables['path'], $variables['options'])) . '"' . drupal_attributes($variables['options']['attributes']) . '>' . ($variables['options']['html'] ? $variables['text'] : check_plain($variables['text'])) . '</a>';
}

/**
 * Remove the search form on the search results page.
 */
function fz_rijkshuisstijl_form_search_form_alter(&$form, &$form_state) {
  $args = arg();
  if ($args[0] == 'search' && !empty($args[2])) {
    $form['#access'] = FALSE;
  }
}

/**
 * Adding a search count above the search results
 */
function fz_rijkshuisstijl_preprocess_search_results(&$variables) {
  // Extract keys as remainder of path
  // Note: support old GET format of searches for existing links.
  $path = explode('/', $_GET['q'], 3);
  $keys = empty($_REQUEST['keys']) ? '' : $_REQUEST['keys'];
  $keys = count($path) == 3 ? $path[2] : $keys;

  if (isset($GLOBALS['pager_total_items'])) {
    // Define the number of results being shown on a page.
    $items_per_page = 10;
    $current_page = 1;

    // Get the current page.
    if (isset($_REQUEST['page'])) {
      $current_page = $_REQUEST['page'] + 1;
    }

    // Get the total number of results from the $GLOBALS.
    $total = $GLOBALS['pager_total_items'][0];

    // Perform calculation.
    $start = 10 * $current_page - 9;
    $end = $items_per_page * $current_page;
    if ($end > $total) {
      $end = $total;
    }

    // Set this html to the $variables.
    $variables['rijkshuisstijl_search_totals'] = '<p class="search-summary"><strong>' . t('@start to @end of @total search results', array(
      '@start' => $start,
      '@end' => $end,
      '@total' => $total,
    )) . '</strong></p>';
  }
  else {
    $variables['rijkshuisstijl_search_totals'] = '';
  }
}
