<?php
/**
 * @file
 * Display Suite Project template.
 *
 * Available variables:
 *
 * Layout:
 * - $classes: String of classes that can be used to style this layout.
 * - $contextual_links: Renderable array of contextual links.
 *
 * Regions:
 *
 * - $bare_image_content: Rendered content for the "Content" region.
 */
?>

<?php if ($bare_image_content): ?>
  <?php print $bare_image_content; ?>
<?php endif; ?>
