<?php
/**
 * @file
 * Module with functions for Forensische zorg theme.
 * All functions in this file should be moved to the base theme.
 */

/**
 * We override this function because we add the 'external' class using
 * theme_link instead of theme_menu_link.
 */
function fz_rijkshuisstijl_menu_link($variables) {
  return theme_menu_link($variables);
}

/**
 * Implements theme_webform_email
 * We override this function because the XHTML DOCTYPE doesn't
 * allow <input type="email" />.
 */
function fz_rijkshuisstijl_webform_email($variables) {
  $element = &$variables['element'];

  // This IF statement is mostly in place to allow our tests to set type="text"
  // because SimpleTest does not support type="email".
  if (!isset($element['#attributes']['type'])) {
    $element['#attributes']['type'] = 'text';
  }

  return theme_webform_email($variables);
}

/**
 * Implements theme_menu_link().
 * Override links for menu blocks.
 *
 * Only do this because the rijkshuisstijl theme misses last_item.
 */
function fz_rijkshuisstijl_menu_link__menu_block($variables) {
  $attributes = $variables['element']['#attributes'];
  $element = $variables['element'];
  $element['#attributes']['class'] = array();

  if (in_array('first', $attributes['class'])) {
    $element['#attributes']['class'][] = 'first_item';
  }

  if (in_array('last', $attributes['class'])) {
    $element['#attributes']['class'][] = 'last_item';
  }

  if (in_array('active', $attributes['class'])) {
    $element['#attributes']['class'][] = 'active';
  }

  $sub_menu = '';
  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
    $element['#attributes']['class'][] = 'open';
  }
  elseif ($element['#below'] == false && in_array('active-trail', $attributes['class'])) {
    $element['#attributes']['class'][] = 'active';
  }

  if (empty($element['#attributes']['class'])) {
    unset($element['#attributes']['class']);
  }

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>" . PHP_EOL;
}
