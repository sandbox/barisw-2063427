<?php
/**
 * @file
 * Display the JW Player.
 *
 * Variables available:
 * - $html_id: Unique id generated for each video.
 * - $videos: Array with one or more video files, keyed by mime-type.
 * - $caption: Path to the subtitle file, if available.
 * - $audiotrack: Path to the audio transcription, if available.
 * - $toggles: Array with download links.
 * - $width: Width of the video player.
 * - $height: Height of the video player.
 * - $poster: URL to an image to be used for the preview image of this video.
 * - $jw_player_inline_js_code: JSON data with settings for the video player.
 * - $files: all available files. Not used, but you can use them if you prefer.
 *
 * @see template_preprocess_jw_player()
 */
?>
<div class="toggle-wrapper">
  <?php if(isset($poster)) : ?>
    <img class="toggle-main" id="<?php print $html_id ?>" src="<?php print $poster ?>" width="<?php print $width ?>" height="<?php print $height ?>" alt="" />
  <?php endif ?>
  <?php if(isset($toggles)): ?>
    <h2 class="element-invisible"><?php print(t('Additional information')); ?></h2>
    <?php
      print theme('item_list', array(
        'items' => $toggles,
        'attributes' => array('class' => array('extras')),
      ));
    ?>
  <?php endif ?>
</div>
<script type="text/javascript">
  jwplayer('<?php print $html_id ?>').setup(<?php print $jw_player_inline_js_code?>);
</script>
