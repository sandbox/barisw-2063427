<?php

/**
 * @file
 * Display Suite Project configuration.
 */

function ds_bare_image() {
  return array(
    'label' => t('Bare image'),
    'regions' => array(
      'bare_image_content' => t('Content'),
    ),
  );
}
