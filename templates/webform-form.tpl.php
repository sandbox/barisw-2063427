<?php

/**
 * @file
 * Customize the display of a complete webform.
 *
 * This file may be renamed "webform-form-[nid].tpl.php" to target a specific
 * webform on your site. Or you can leave it "webform-form.tpl.php" to affect
 * all webforms on your site.
 *
 * Available variables:
 * - $form: The complete form array.
 * - $nid: The node ID of the Webform.
 *
 * The $form array contains two main pieces:
 * - $form['submitted']: The main content of the user-created form.
 * - $form['details']: Internal information stored by Webform.
 */
?>
<?php
  // If editing or viewing submissions, display the navigation at the top.
  if (isset($form['submission_info']) || isset($form['navigation'])) {
    print drupal_render($form['navigation']);
    print drupal_render($form['submission_info']);
  }

  $notice_is_set = FALSE;
  foreach ($form['submitted'] as &$form_element) {
    // Add the Required notice on the first fieldset.
    if (!$notice_is_set && isset($form_element['#type']) && $form_element['#type'] == 'fieldset') {
      $form_element['#prefix'] = '<p class="mandatory">' . t('Required fields are marked with a !requiredfield', array('!requiredfield' => '<em title="' . t('required field') . '">*</em>')) . '</p>';
      $notice_is_set = TRUE;
    }
  }
  print drupal_render($form['submitted']);

  // Always print out the entire $form. This renders the remaining pieces of the
  // form that haven't yet been rendered above.
  print drupal_render_children($form);

  // Print out the navigation again at the bottom.
  if (isset($form['submission_info']) || isset($form['navigation'])) {
    unset($form['navigation']['#printed']);
    print drupal_render($form['navigation']);
  }
