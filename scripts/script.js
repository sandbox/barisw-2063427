/**
 * Forensische zorg scripts.
 */

(function ($) {
  // Add behavior for front page buttons.
  Drupal.behaviors.frontPageButtonText = {
    attach: function (context, settings) {
      if ($(window).width() > 768) {
        $('.front-page-buttons .body').hide();
        $('.front-page-buttons .box', context).hover(
          function(){
            $(this).find('.body').fadeIn();
          },
          function(){
            $(this).find('.body').fadeOut();
          }
        );
      }
      $('.front-page-buttons .box').css("cursor", "pointer").click(function(){
        window.location=$(this).find(":header a").attr("href");
        return false;
      });
    }
  };

  // Add print link.
  Drupal.behaviors.addPrintLink = {
    attach: function (context, settings) {
      $('.extralinks').prepend('<div class="print-link"><a href="#" title="' + Drupal.t('Print page') + '"><span class="imgreplacement"></span>' + Drupal.t('Print') + '</a></div><div class="sl-wrapper"></div>');
      $('.extralinks .label-inline').appendTo('.extralinks .sl-wrapper');
      $('.extralinks .service-links').appendTo('.extralinks .sl-wrapper');
      $('.print-link a').click(function(){
        window.print();
        return false;
      });
    }
  };

  // Add external links to list items.
  Drupal.behaviors.addExternalClass = {
    attach: function (context, settings) {
      $('a.is-external').parent('li').addClass('external');
    }
  };

  // Add toggle behaviour to create toggles.
  Drupal.behaviors.addToggle = {
    attach: function (context, settings) {
      $('.toggle-wrapper').each(function(){
        $(this).find('.extras').width($(this).find('.toggle-main').width());
      });

      $('.toggle-wrapper .toggle :header').each(function(){
        $(this).html('<a href="#">' + $(this).text() + '<span class="facet-button"></span></a>');
      });

      $('.toggle-wrapper :header a').click(function(event){
        $(this).toggleClass('open');
        $(this).parents('.toggle').toggleClass('open');
        return false;
      });
    }
  };

}(jQuery));

